//1.
const arrayNames = ['Alex','Vanya','Tanya','Lena','Tolya'];
function changeRegistr(arr, names) {
    let arrSmallNames = [];
    for (let val of arr){
        arrSmallNames.push(names(val));
    } return arrSmallNames;
}
console.log(changeRegistr(arrayNames, newNames => newNames.toLowerCase()));


//2.
let arrayNumbers = [44, 12, 11, 7, 1, 99, 43, 5, 69]
console.log(arrayNumbers);
function test(massiv, sort){
    let k = massiv.length;
    for (let i = 1; i < k; i++){
        for (let a = 0; a < i; a++){
            if(sort(massiv[i], massiv[a])){
                let buffer = massiv[i];
                massiv[i] = massiv[a];
                massiv[a] = buffer;  
            }
        }
    }    
    return massiv;
}
let a = test(arrayNumbers, (i, a) => i < a);
console.log(a);



//3.
class User{
    constructor(firstName, secondName, lastName, age, receiptDate, dateOfIssue, speciality){
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.age = age;
        this.receiptDate = receiptDate;
        this.dateOfIssue = dateOfIssue;
        this.speciality = speciality;
    }
    get birthDay(){
        let p = (`Возраст студента  ${new Date().getFullYear() - this.age}`);
        return p;
    }
    smallName(){
        let q = (`ФИО: ${this.lastName} ${this.firstName[0]}.${this.secondName[0]}`);
        return q;
    }
    largeLetter(){
        console.log(this.firstName, this.secondName, this.lastName)
        return (`${this.firstName[0].toUpperCase() + this.firstName.slice(1)} ${this.secondName[0].toUpperCase() + this.secondName.slice(1)} ${this.lastName[0].toUpperCase() + this.lastName.slice(1)}`);
    }
    studyYear(){
        let yearOfStudy = this.dateOfIssue - this.receiptDate;
        return yearOfStudy;
    }
    studyMonth(){
        let monthOfStudy = (this.dateOfIssue - this.receiptDate)*12;
        return monthOfStudy;
    }
}

class Student extends User {
    constructor(grade, firstName, secondName, lastName, age, receiptDate, dateOfIssue, speciality){
        super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, speciality)
     this.grade = grade;
}
diploma(){
    return (this.grade < 60? 'Диплома нет':'Диплом')
}
}
class PreparatoryGroup extends User {
    constructor(issueOrDeduction, firstName, secondName, lastName, age, receiptDate, dateOfIssue, speciality){
        super(firstName, secondName, lastName, age, receiptDate, dateOfIssue, speciality)
        this.issueOrDeduction = issueOrDeduction;
    }
}

let student01 = new Student(80, 'иван','сергеевич','дудка', 2000, 2019,2021,'engineer')
let student02 = new Student(95, 'Светлана','Викторовна','Грог',1999,2018,2021,'engineer',)
let student03 = new PreparatoryGroup(true, 'Илья','Андреевич','Золя',2001,2021,2021,'',)
let student04 = new Student(85, 'Сергей','Степанович','Кроль',1998,2017,2021,'engineer',)
let student05 = new Student(70, 'Анна','Игоревна','Лапко',2000,2019,2021,'engineer',)
let student06 = new PreparatoryGroup(true, 'Дмитрий','Валерьевич','Сушко',2001,2021,2021,'',)
// console.log(student01, student02, student03, student04, student05, student06)
console.log(student05.smallName());
console.log(student01.largeLetter());
console.log(student05.studyYear());
console.log(student05.studyMonth());
console.log(student05.birthDay);
console.log(student02.diploma())
